package ru.maramzin.shopapp.storage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The type Storage application.
 */
@SpringBootApplication
public class StorageApplication {

  /**
   * The entry point of application.
   *
   * @param args the input arguments
   */
  public static void main(String[] args) {
    SpringApplication.run(StorageApplication.class, args);
  }

}
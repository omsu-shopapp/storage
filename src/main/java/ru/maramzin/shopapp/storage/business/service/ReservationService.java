package ru.maramzin.shopapp.storage.business.service;

import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maramzin.shopapp.storage.data.dto.operation.purchase.reservation.ReservationResponse;
import ru.maramzin.shopapp.storage.data.dto.operation.purchase.reservation.product.ReservedProductRequest;
import ru.maramzin.shopapp.storage.data.dto.operation.purchase.reservation.product.ReservedProductResponse;
import ru.maramzin.shopapp.storage.data.entity.Reservation;
import ru.maramzin.shopapp.storage.data.entity.ReservedProduct;
import ru.maramzin.shopapp.storage.data.entity.enumeration.ReservationStatus;
import ru.maramzin.shopapp.storage.data.repository.ReservationRepository;
import ru.maramzin.shopapp.storage.data.repository.ReservedProductRepository;
import ru.maramzin.shopapp.storage.utils.exception.ReservationException;
import ru.maramzin.shopapp.storage.utils.exception.ValidationException;
import ru.maramzin.shopapp.storage.utils.logs.ExceptionMessages;
import ru.maramzin.shopapp.storage.utils.mapper.ReservationMapper;
import ru.maramzin.shopapp.storage.utils.mapper.ReservedProductMapper;

/**
 * The type Reservation service.
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class ReservationService {

  private final ReservationRepository reservationRepository;
  private final ReservedProductRepository reservedProductRepository;
  private final ReservationMapper reservationMapper;
  private final ReservedProductMapper reservedProductMapper;

  /**
   * Create reservation.
   *
   * @param clientId the client id
   * @return the uuid
   */
  @Transactional
  public ReservationResponse create(UUID clientId) {
    Reservation reservation = Reservation.builder()
        .id(UUID.randomUUID())
        .clientId(clientId)
        .status(ReservationStatus.OPENED)
        .build();

    reservationRepository.save(reservation);
    log.info("Reservation has been created: {}", reservation);

    return reservationMapper.mapEntityToResponse(reservation);
  }

  /**
   * Add reserved product to reservation.
   *
   * @param request the request
   * @return the reserved product response
   */
  @Transactional
  public ReservedProductResponse addReservedProduct(ReservedProductRequest request) {
    ReservedProduct reservedProduct = reservedProductMapper.mapRequestToEntity(request);
    reservedProductRepository.save(reservedProduct);

    log.info("Product has been reserved. Info: {}", reservedProduct);
    return reservedProductMapper.mapEntityToResponse(reservedProduct);
  }

  /**
   * Update status.
   *
   * @param reservationId the reservation id
   * @param status        the status
   * @return the reservation response
   */
  @Transactional
  public ReservationResponse updateStatus(UUID reservationId, ReservationStatus status) {
    Reservation reservation = reservationRepository.findById(reservationId)
        .orElseThrow(() -> new ReservationException(
            String.format(ExceptionMessages.RESERVATION_NOT_FOUND_BY_ID, reservationId)));

    if (reservation.getStatus() != ReservationStatus.OPENED) {
      throw new ValidationException(ExceptionMessages.RESERVATION_NOT_OPENED);
    }

    reservation.setStatus(status);
    reservationRepository.save(reservation);

    log.info("Reservation({}) status has been updated from OPENED to {}",
        reservation, status.name());

    return reservationMapper.mapEntityToResponse(reservation);
  }

  /**
   * Find by id reservation response.
   *
   * @param id the id
   * @return the reservation response
   */
  @Transactional
  public ReservationResponse findById(UUID id) {
    Reservation reservation = reservationRepository.findById(id)
        .orElseThrow(() -> new ReservationException(
            String.format(ExceptionMessages.RESERVATION_NOT_FOUND_BY_ID, id)));

    log.info("Reservation({}) found by id", reservation);
    return reservationMapper.mapEntityToResponse(reservation);
  }
}

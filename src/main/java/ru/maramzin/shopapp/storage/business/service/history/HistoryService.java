package ru.maramzin.shopapp.storage.business.service.history;

import java.util.List;
import java.util.UUID;

/**
 * The interface History service.
 *
 * @param <Q> the type parameter
 * @param <S> the type parameter
 */
public interface HistoryService<Q, S> {

  /**
   * Add s.
   *
   * @param request the request
   * @return the s
   */
  S add(Q request);

  /**
   * Find by product info id list.
   *
   * @param productInfoId the product info id
   * @return the list
   */
  List<S> findByProductInfoId(UUID productInfoId);
}

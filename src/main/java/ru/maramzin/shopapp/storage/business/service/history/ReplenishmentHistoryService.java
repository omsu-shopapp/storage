package ru.maramzin.shopapp.storage.business.service.history;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maramzin.shopapp.storage.data.dto.history.replenishment.ReplenishmentHistoryRequest;
import ru.maramzin.shopapp.storage.data.dto.history.replenishment.ReplenishmentHistoryResponse;
import ru.maramzin.shopapp.storage.data.entity.ReplenishmentHistory;
import ru.maramzin.shopapp.storage.data.repository.ReplenishmentHistoryRepository;
import ru.maramzin.shopapp.storage.utils.mapper.ReplenishmentHistoryMapper;

/**
 * The type Replenishment history service.
 */
@RequiredArgsConstructor
@Service
@Slf4j
public class ReplenishmentHistoryService implements
    HistoryService<ReplenishmentHistoryRequest, ReplenishmentHistoryResponse> {

  private final ReplenishmentHistoryRepository repository;
  private final ReplenishmentHistoryMapper mapper;

  @Transactional
  @Override
  public ReplenishmentHistoryResponse add(ReplenishmentHistoryRequest request) {
    ReplenishmentHistory history = mapper.mapRequestToEntity(request);
    history.setId(UUID.randomUUID());
    history.setDatetime(LocalDateTime.now());
    repository.save(history);

    log.info("Replenishment history has been saved to repository {}", history);

    return mapper.mapEntityToResponse(history);
  }

  @Transactional
  @Override
  public List<ReplenishmentHistoryResponse> findByProductInfoId(UUID productInfoId) {
    List<ReplenishmentHistory> histories = repository.findAllByProductInfoId(productInfoId);
    return histories.stream()
        .map(mapper::mapEntityToResponse)
        .toList();
  }
}

package ru.maramzin.shopapp.storage.data.dto.history.discount;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import ru.maramzin.shopapp.storage.data.dto.history.BaseHistoryRequest;

/**
 * The type Discount history request.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
public class DiscountHistoryRequest extends BaseHistoryRequest {

  private Integer discountBefore;
  private Integer discountAfter;
}

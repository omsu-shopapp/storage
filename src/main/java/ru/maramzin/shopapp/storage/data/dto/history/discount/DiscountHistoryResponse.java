package ru.maramzin.shopapp.storage.data.dto.history.discount;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import ru.maramzin.shopapp.storage.data.dto.history.BaseHistoryResponse;

/**
 * The type Discount history response.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
public class DiscountHistoryResponse extends BaseHistoryResponse {

  private Integer discountBefore;
  private Integer discountAfter;
}

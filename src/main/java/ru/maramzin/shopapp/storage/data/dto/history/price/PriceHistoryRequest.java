package ru.maramzin.shopapp.storage.data.dto.history.price;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import ru.maramzin.shopapp.storage.data.dto.history.BaseHistoryRequest;

/**
 * The type Price history request.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
public class PriceHistoryRequest extends BaseHistoryRequest {

  private Integer costBefore;
  private Integer costAfter;
}

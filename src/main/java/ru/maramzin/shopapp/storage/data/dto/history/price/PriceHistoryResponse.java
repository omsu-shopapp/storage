package ru.maramzin.shopapp.storage.data.dto.history.price;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import ru.maramzin.shopapp.storage.data.dto.history.BaseHistoryResponse;

/**
 * The type Price history response.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
public class PriceHistoryResponse extends BaseHistoryResponse {

  private Integer costBefore;
  private Integer costAfter;
}

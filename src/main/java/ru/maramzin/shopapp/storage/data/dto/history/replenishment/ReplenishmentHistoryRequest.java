package ru.maramzin.shopapp.storage.data.dto.history.replenishment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import ru.maramzin.shopapp.storage.data.dto.history.BaseHistoryRequest;

/**
 * The type Selling history request.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
public class ReplenishmentHistoryRequest extends BaseHistoryRequest {

  private Integer quantity;
}

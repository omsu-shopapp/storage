package ru.maramzin.shopapp.storage.data.dto.history.replenishment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import ru.maramzin.shopapp.storage.data.dto.history.BaseHistoryResponse;

/**
 * The type Replenishment history response.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
public class ReplenishmentHistoryResponse extends BaseHistoryResponse {

  private Integer quantity;
}

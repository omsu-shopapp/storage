package ru.maramzin.shopapp.storage.data.dto.history.selling;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import ru.maramzin.shopapp.storage.data.dto.history.BaseHistoryResponse;
import ru.maramzin.shopapp.storage.data.entity.enumeration.CashOutStatus;

/**
 * The type Selling history response.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
public class SellingHistoryResponse extends BaseHistoryResponse {

  private Integer quantity;
  private Integer cashOut;
  private CashOutStatus cashOutStatus;
}

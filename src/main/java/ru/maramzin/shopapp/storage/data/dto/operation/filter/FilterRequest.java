package ru.maramzin.shopapp.storage.data.dto.operation.filter;

import java.util.UUID;
import lombok.Builder;
import lombok.Data;
import ru.maramzin.shopapp.storage.data.entity.enumeration.ClothesCategory;
import ru.maramzin.shopapp.storage.data.entity.enumeration.SeasonCategory;

/**
 * The type Filter request.
 */
@Data
@Builder
public class FilterRequest {

  private final String name;
  private final Integer costFrom;
  private final Integer costTo;
  private final Integer discountFrom;
  private final Integer discountTo;
  private final ClothesCategory clothesCategory;
  private final SeasonCategory seasonCategory;
  private final UUID sellerId;
}

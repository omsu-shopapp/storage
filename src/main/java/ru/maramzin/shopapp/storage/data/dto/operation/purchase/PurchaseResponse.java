package ru.maramzin.shopapp.storage.data.dto.operation.purchase;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.PositiveOrZero;
import java.util.UUID;
import lombok.Builder;
import lombok.Data;

/**
 * The type Purchase response.
 */
@Data
@Builder
public class PurchaseResponse {

  @NotNull
  private final UUID productId;

  @NotBlank
  private final String productName;

  @PositiveOrZero
  private final Integer cost;

  @PositiveOrZero
  @Max(value = 100)
  private final Integer discount;

  @Positive
  private final Integer quantity;
}

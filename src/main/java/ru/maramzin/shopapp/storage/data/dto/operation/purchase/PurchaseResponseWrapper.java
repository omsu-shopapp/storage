package ru.maramzin.shopapp.storage.data.dto.operation.purchase;

import java.util.List;
import java.util.UUID;
import lombok.Builder;
import lombok.Data;

/**
 * The type Purchase response wrapper.
 */
@Data
@Builder
public class PurchaseResponseWrapper {

  private List<PurchaseResponse> content;
  private UUID reservationId;
}

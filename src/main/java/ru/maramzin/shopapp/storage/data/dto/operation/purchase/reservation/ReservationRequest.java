package ru.maramzin.shopapp.storage.data.dto.operation.purchase.reservation;

import lombok.Builder;
import lombok.Data;

/**
 * The type Reservation request.
 */
@Data
@Builder
public class ReservationRequest {

}

package ru.maramzin.shopapp.storage.data.dto.operation.purchase.reservation;

import java.util.List;
import java.util.UUID;
import lombok.Builder;
import lombok.Data;
import ru.maramzin.shopapp.storage.data.dto.operation.purchase.reservation.product.ReservedProductResponse;
import ru.maramzin.shopapp.storage.data.entity.enumeration.ReservationStatus;

/**
 * The type Reservation response.
 */
@Data
@Builder
public class ReservationResponse {

  private UUID id;

  private UUID clientId;

  private ReservationStatus status;

  private List<ReservedProductResponse> reservedProducts;
}

package ru.maramzin.shopapp.storage.data.dto.operation.purchase.reservation.product;

import java.util.UUID;
import lombok.Builder;
import lombok.Data;

/**
 * The type Reserved product request.
 */
@Data
@Builder
public class ReservedProductRequest {

  private String productName;
  private Integer quantity;
  private UUID reservationId;
}

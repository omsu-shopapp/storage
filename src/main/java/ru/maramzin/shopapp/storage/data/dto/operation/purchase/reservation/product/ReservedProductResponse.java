package ru.maramzin.shopapp.storage.data.dto.operation.purchase.reservation.product;

import lombok.Builder;
import lombok.Data;
import ru.maramzin.shopapp.storage.data.dto.product.ProductInfoResponse;

/**
 * The type Reserved product response.
 */
@Data
@Builder
public class ReservedProductResponse {

  private ProductInfoResponse productInfo;
  private Integer quantity;
}

package ru.maramzin.shopapp.storage.data.dto.product;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.PositiveOrZero;
import java.util.UUID;
import lombok.Builder;
import lombok.Data;
import ru.maramzin.shopapp.storage.data.entity.enumeration.ClothesCategory;
import ru.maramzin.shopapp.storage.data.entity.enumeration.SeasonCategory;

/**
 * The type Product info request.
 */
@Data
@Builder
public class ProductInfoRequest {

  @NotBlank
  private final String name;
  private final String description;

  @NotNull
  @Positive
  private final Integer cost;

  @NotNull
  @PositiveOrZero
  private final Integer discount;

  @NotNull
  @PositiveOrZero
  private final Integer quantity;

  @NotNull
  private final ClothesCategory clothesCategory;

  @NotNull
  private final SeasonCategory seasonCategory;

  @NotNull
  private final UUID sellerId;
}

package ru.maramzin.shopapp.storage.data.dto.product;

import java.util.UUID;
import lombok.Builder;
import lombok.Data;
import ru.maramzin.shopapp.storage.data.entity.enumeration.ClothesCategory;
import ru.maramzin.shopapp.storage.data.entity.enumeration.SeasonCategory;

/**
 * The type Product info response.
 */
@Data
@Builder
public class ProductInfoResponse {

  private final UUID id;
  private final String name;
  private final String description;
  private final Integer cost;
  private final Integer discount;
  private final Integer quantity;
  private final ClothesCategory clothesCategory;
  private final SeasonCategory seasonCategory;
  private final UUID sellerId;
}

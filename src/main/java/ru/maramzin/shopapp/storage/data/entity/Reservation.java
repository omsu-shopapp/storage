package ru.maramzin.shopapp.storage.data.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import java.util.List;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.maramzin.shopapp.storage.data.entity.enumeration.ReservationStatus;

/**
 * The type Reservation.
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@EqualsAndHashCode
@ToString
@Table(name = "reservation")
@Entity
public class Reservation {

  @Id
  private UUID id;

  private UUID clientId;

  private ReservationStatus status;

  @ToString.Exclude
  @OneToMany(mappedBy = "reservation", cascade = CascadeType.ALL)
  private List<ReservedProduct> reservedProducts;
}

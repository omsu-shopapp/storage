package ru.maramzin.shopapp.storage.data.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import ru.maramzin.shopapp.storage.data.entity.enumeration.CashOutStatus;

/**
 * The type Selling history.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@ToString
@Entity
@Table(name = "selling_history")
public class SellingHistory extends BaseHistory {

  private Integer quantity;
  private Integer cashOut;

  @Enumerated(EnumType.STRING)
  private CashOutStatus cashOutStatus;
}

package ru.maramzin.shopapp.storage.data.entity.enumeration;

public enum CashOutStatus {
  ISSUED, NOT_ISSUED
}

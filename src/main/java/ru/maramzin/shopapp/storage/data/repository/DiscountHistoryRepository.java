package ru.maramzin.shopapp.storage.data.repository;

import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.maramzin.shopapp.storage.data.entity.DiscountHistory;

/**
 * The interface Discount changing history repository.
 */
public interface DiscountHistoryRepository extends JpaRepository<DiscountHistory, UUID> {

  List<DiscountHistory> findAllByProductInfoId(UUID productInfoId);
}

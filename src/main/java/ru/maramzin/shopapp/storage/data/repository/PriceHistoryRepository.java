package ru.maramzin.shopapp.storage.data.repository;

import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.maramzin.shopapp.storage.data.entity.PriceHistory;

/**
 * The interface Price changing history repository.
 */
public interface PriceHistoryRepository extends JpaRepository<PriceHistory, UUID> {

  List<PriceHistory> findAllByProductInfoId(UUID productInfoId);
}

package ru.maramzin.shopapp.storage.data.repository;

import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.maramzin.shopapp.storage.data.entity.ReplenishmentHistory;

/**
 * The interface Replenishment history repository.
 */
public interface ReplenishmentHistoryRepository extends JpaRepository<ReplenishmentHistory, UUID> {

  List<ReplenishmentHistory> findAllByProductInfoId(UUID productInfoId);
}

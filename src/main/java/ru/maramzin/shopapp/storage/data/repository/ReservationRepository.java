package ru.maramzin.shopapp.storage.data.repository;

import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.maramzin.shopapp.storage.data.entity.Reservation;

/**
 * The interface Reservation repository.
 */
public interface ReservationRepository extends JpaRepository<Reservation, UUID> {

}

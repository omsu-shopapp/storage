package ru.maramzin.shopapp.storage.egress.handler;

import jakarta.validation.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.maramzin.shopapp.storage.utils.exception.ExceptionDto;
import ru.maramzin.shopapp.storage.utils.exception.ProductInfoException;
import ru.maramzin.shopapp.storage.utils.exception.ValidationException;

import java.time.LocalDateTime;

/**
 * The type Product exception handler.
 */
@ControllerAdvice
public class ProductExceptionHandler {

  /**
   * Handle product info exception response entity.
   *
   * @param cause the cause
   * @return the response entity
   */
  @ExceptionHandler(ProductInfoException.class)
  public ResponseEntity<ExceptionDto> handleProductInfoException(ProductInfoException cause) {
    return new ResponseEntity<>(cause.getExceptionDto(), HttpStatus.NOT_FOUND);
  }

  /**
   * Handle not enough quantity exception response entity.
   *
   * @param cause the cause
   * @return the response entity
   */
  @ExceptionHandler(ValidationException.class)
  public ResponseEntity<ExceptionDto> handleValidationException(ValidationException cause) {
    return new ResponseEntity<>(cause.getExceptionDto(), HttpStatus.BAD_REQUEST);
  }

  /**
   * Exception handler response entity.
   *
   * @param e the exception
   * @return the response entity
   */
  @ExceptionHandler(jakarta.validation.ValidationException.class)
  public ResponseEntity<ExceptionDto> exceptionHandler(ValidationException e) {
    return new ResponseEntity<>(new ExceptionDto(processMessage(e.getMessage()), LocalDateTime.now()),
            HttpStatus.BAD_REQUEST);
  }

  /**
   * Exception handler response entity.
   *
   * @param e the exception
   * @return the response entity
   */
  @ExceptionHandler(ConstraintViolationException.class)
  public ResponseEntity<ExceptionDto> exceptionHandler(ConstraintViolationException e) {
    return new ResponseEntity<>(new ExceptionDto(processMessage(e.getMessage()), LocalDateTime.now()),
            HttpStatus.BAD_REQUEST);
  }

  private String processMessage(String message) {
    return message.replaceAll("filter.", "");
  }
}

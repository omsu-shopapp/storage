package ru.maramzin.shopapp.storage.ingress.controller;

import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.maramzin.shopapp.storage.business.facade.ProductFacade;
import ru.maramzin.shopapp.storage.data.dto.history.price.PriceHistoryResponse;

/**
 * The type Price history controller.
 */
@RequestMapping("histories/price")
@RequiredArgsConstructor
@RestController
@Slf4j
public class PriceHistoryController {

  private final ProductFacade facade;

  /**
   * Find list.
   *
   * @param productName the product name
   * @return the list
   */
  @GetMapping("/find")
  public List<PriceHistoryResponse> find(@RequestParam String productName) {
    log.info("REST request to find price histories by product name: {}", productName);
    return facade.findPriceHistoriesByProductName(productName);
  }
}

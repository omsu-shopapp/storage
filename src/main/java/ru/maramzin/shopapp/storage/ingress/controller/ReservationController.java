package ru.maramzin.shopapp.storage.ingress.controller;

import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.maramzin.shopapp.storage.business.service.ReservationService;
import ru.maramzin.shopapp.storage.data.dto.operation.purchase.reservation.ReservationResponse;

/**
 * The type Reservation controller.
 */
@Slf4j
@Validated
@RequestMapping("/reservation")
@RequiredArgsConstructor
@RestController
public class ReservationController {

  private final ReservationService service;

  /**
   * Find reservation response.
   *
   * @param id the id
   * @return the reservation response
   */
  @GetMapping("/find")
  public ReservationResponse find(@RequestParam UUID id) {
    log.info("REST request to find reservation info by id: {}", id);
    return service.findById(id);
  }
}

package ru.maramzin.shopapp.storage.utils.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ExceptionDto {

    private String errorDescription;

    private LocalDateTime timestamp;
}
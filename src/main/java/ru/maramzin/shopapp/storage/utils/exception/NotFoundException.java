package ru.maramzin.shopapp.storage.utils.exception;

import lombok.Getter;

@Getter
public class NotFoundException extends ExtendedException {

  public NotFoundException(String message) {
    super(message);
  }

  public NotFoundException(ExceptionDto exceptionDto) {
    super(exceptionDto);
  }
}

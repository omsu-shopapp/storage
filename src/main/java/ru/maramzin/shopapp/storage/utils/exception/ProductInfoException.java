package ru.maramzin.shopapp.storage.utils.exception;

/**
 * The type Product info exception.
 */
public class ProductInfoException extends ExtendedException {

  /**
   * Instantiates a new Product info exception.
   *
   * @param message the message
   */
  public ProductInfoException(String message) {
    super(message);
  }
}

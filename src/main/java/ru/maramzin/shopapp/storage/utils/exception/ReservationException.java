package ru.maramzin.shopapp.storage.utils.exception;

/**
 * The type Reservation exception.
 */
public class ReservationException extends RuntimeException {

  /**
   * Instantiates a new Reservation exception.
   *
   * @param message the message
   */
  public ReservationException(String message) {
    super(message);
  }
}

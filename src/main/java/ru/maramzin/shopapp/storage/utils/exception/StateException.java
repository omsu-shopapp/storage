package ru.maramzin.shopapp.storage.utils.exception;

import lombok.Getter;

@Getter
public class StateException extends ExtendedException {

    public StateException(String message) {
        super(message);
    }

    public StateException(ExceptionDto exceptionDto) {
        super(exceptionDto);
    }
}

package ru.maramzin.shopapp.storage.utils.exception;

import lombok.Getter;

@Getter
public class ValidationException extends ExtendedException {

    public ValidationException(String message) {
        super(message);
    }

    public ValidationException(ExceptionDto exceptionDto) {
        super(exceptionDto);
    }
}

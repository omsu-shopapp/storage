package ru.maramzin.shopapp.storage.utils.logs;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import ru.maramzin.shopapp.storage.data.dto.operation.filter.FilterRequest;

/**
 * The type Filter map generator.
 */
public class FilterMapGenerator {

  /**
   * Generate filter map map.
   *
   * @param request the request
   * @return the map
   */
  public static Map<String, Object> generateFilterMap(FilterRequest request) {

    Map<String, Object> filterMap = new HashMap<>();
    Field[] fields = FilterRequest.class.getDeclaredFields();

    Arrays.stream(fields).forEach(field -> {
      field.setAccessible(true);
      String name = field.getName();
      Object value;
      try {
        value = field.get(request);
      } catch (IllegalAccessException e) {
        return;
      }

      if (value != null) {
        filterMap.put(name, value);
      }
    });

    return filterMap;
  }

}

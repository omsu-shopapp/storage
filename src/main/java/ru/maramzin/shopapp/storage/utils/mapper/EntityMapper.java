package ru.maramzin.shopapp.storage.utils.mapper;

/**
 * The interface Entity mapper.
 *
 * @param <E> Entity class
 * @param <Q> Request class
 * @param <S> Response class
 */
public interface EntityMapper<E, Q, S> {

  /**
   * Map entity to request e.
   *
   * @param request the request
   * @return the e
   */
  E mapRequestToEntity(Q request);

  /**
   * Map entity to request q.
   *
   * @param entity the entity
   * @return the q
   */
  Q mapEntityToRequest(E entity);

  /**
   * Map response to entity e.
   *
   * @param response the response
   * @return the e
   */
  E mapResponseToEntity(S response);

  /**
   * Map entity to response s.
   *
   * @param entity the entity
   * @return the s
   */
  S mapEntityToResponse(E entity);
}

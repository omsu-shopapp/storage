package ru.maramzin.shopapp.storage.utils.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.storage.data.dto.history.price.PriceHistoryRequest;
import ru.maramzin.shopapp.storage.data.dto.history.price.PriceHistoryResponse;
import ru.maramzin.shopapp.storage.data.entity.PriceHistory;

/**
 * The type Price history mapper.
 */
@RequiredArgsConstructor
@Component
public class PriceHistoryMapper implements
    EntityMapper<PriceHistory, PriceHistoryRequest, PriceHistoryResponse> {

  private final ObjectMapper mapper;

  @Override
  public PriceHistory mapRequestToEntity(PriceHistoryRequest request) {
    return mapper.convertValue(request, PriceHistory.class);
  }

  @Override
  public PriceHistoryRequest mapEntityToRequest(PriceHistory entity) {
    return mapper.convertValue(entity, PriceHistoryRequest.class);
  }

  @Override
  public PriceHistory mapResponseToEntity(PriceHistoryResponse response) {
    return mapper.convertValue(response, PriceHistory.class);
  }

  @Override
  public PriceHistoryResponse mapEntityToResponse(PriceHistory entity) {
    return mapper.convertValue(entity, PriceHistoryResponse.class);
  }
}

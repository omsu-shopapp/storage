package ru.maramzin.shopapp.storage.utils.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.storage.data.dto.product.ProductInfoRequest;
import ru.maramzin.shopapp.storage.data.dto.product.ProductInfoResponse;
import ru.maramzin.shopapp.storage.data.entity.ProductInfo;

/**
 * The type Product info mapper.
 */
@RequiredArgsConstructor
@Component
public class ProductInfoMapper implements
    EntityMapper<ProductInfo, ProductInfoRequest, ProductInfoResponse> {

  private final ObjectMapper mapper;

  public ProductInfo mapRequestToEntity(ProductInfoRequest request) {
    return mapper.convertValue(request, ProductInfo.class);
  }

  @Override
  public ProductInfoRequest mapEntityToRequest(ProductInfo entity) {
    return mapper.convertValue(entity, ProductInfoRequest.class);
  }

  @Override
  public ProductInfo mapResponseToEntity(ProductInfoResponse response) {
    return mapper.convertValue(response, ProductInfo.class);
  }

  @Override
  public ProductInfoResponse mapEntityToResponse(ProductInfo entity) {
    return mapper.convertValue(entity, ProductInfoResponse.class);
  }

}

package ru.maramzin.shopapp.storage.utils.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.storage.data.dto.history.replenishment.ReplenishmentHistoryRequest;
import ru.maramzin.shopapp.storage.data.dto.history.replenishment.ReplenishmentHistoryResponse;
import ru.maramzin.shopapp.storage.data.entity.ReplenishmentHistory;

/**
 * The type Replenishment history mapper.
 */
@RequiredArgsConstructor
@Component
public class ReplenishmentHistoryMapper implements
    EntityMapper<ReplenishmentHistory, ReplenishmentHistoryRequest, ReplenishmentHistoryResponse> {

  private final ObjectMapper mapper;

  @Override
  public ReplenishmentHistory mapRequestToEntity(ReplenishmentHistoryRequest request) {
    return mapper.convertValue(request, ReplenishmentHistory.class);
  }

  @Override
  public ReplenishmentHistoryRequest mapEntityToRequest(ReplenishmentHistory entity) {
    return mapper.convertValue(entity, ReplenishmentHistoryRequest.class);
  }

  @Override
  public ReplenishmentHistory mapResponseToEntity(ReplenishmentHistoryResponse response) {
    return mapper.convertValue(response, ReplenishmentHistory.class);
  }

  @Override
  public ReplenishmentHistoryResponse mapEntityToResponse(ReplenishmentHistory entity) {
    return mapper.convertValue(entity, ReplenishmentHistoryResponse.class);
  }
}

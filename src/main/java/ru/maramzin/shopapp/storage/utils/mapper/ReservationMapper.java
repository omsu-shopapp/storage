package ru.maramzin.shopapp.storage.utils.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.storage.data.dto.operation.purchase.reservation.ReservationRequest;
import ru.maramzin.shopapp.storage.data.dto.operation.purchase.reservation.ReservationResponse;
import ru.maramzin.shopapp.storage.data.dto.operation.purchase.reservation.product.ReservedProductResponse;
import ru.maramzin.shopapp.storage.data.entity.Reservation;
import ru.maramzin.shopapp.storage.data.entity.ReservedProduct;

/**
 * The type Reservation mapper.
 */
@RequiredArgsConstructor
@Component
public class ReservationMapper implements
    EntityMapper<Reservation, ReservationRequest, ReservationResponse> {

  private final ObjectMapper mapper;
  private final ReservedProductMapper reservedProductMapper;

  @Override
  public Reservation mapRequestToEntity(ReservationRequest request) {
    return mapper.convertValue(request, Reservation.class);
  }

  @Override
  public ReservationRequest mapEntityToRequest(Reservation entity) {
    return mapper.convertValue(entity, ReservationRequest.class);
  }

  @Override
  public Reservation mapResponseToEntity(ReservationResponse response) {
    return null;
  }

  @Override
  public ReservationResponse mapEntityToResponse(Reservation entity) {
    List<ReservedProduct> reservedProducts = Optional.ofNullable(entity.getReservedProducts())
        .orElse(Collections.emptyList());
    List<ReservedProductResponse> reservedProductResponses = reservedProducts.stream()
        .map(reservedProductMapper::mapEntityToResponse)
        .toList();

    return ReservationResponse.builder()
        .id(entity.getId())
        .clientId(entity.getClientId())
        .status(entity.getStatus())
        .reservedProducts(reservedProductResponses)
        .build();
  }
}

package ru.maramzin.shopapp.storage.utils.mapper;

import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.storage.data.dto.operation.purchase.reservation.product.ReservedProductRequest;
import ru.maramzin.shopapp.storage.data.dto.operation.purchase.reservation.product.ReservedProductResponse;
import ru.maramzin.shopapp.storage.data.entity.ProductInfo;
import ru.maramzin.shopapp.storage.data.entity.Reservation;
import ru.maramzin.shopapp.storage.data.entity.ReservedProduct;
import ru.maramzin.shopapp.storage.data.repository.ProductInfoRepository;
import ru.maramzin.shopapp.storage.data.repository.ReservationRepository;
import ru.maramzin.shopapp.storage.utils.exception.ProductInfoException;
import ru.maramzin.shopapp.storage.utils.exception.ReservationException;
import ru.maramzin.shopapp.storage.utils.logs.ExceptionMessages;

/**
 * The type Reserved product mapper.
 */
@RequiredArgsConstructor
@Component
public class ReservedProductMapper implements
    EntityMapper<ReservedProduct, ReservedProductRequest, ReservedProductResponse> {

  private final ProductInfoMapper productInfoMapper;
  private final ProductInfoRepository productInfoRepository;
  private final ReservationRepository reservationRepository;

  @Override
  public ReservedProduct mapRequestToEntity(ReservedProductRequest request) {
    ProductInfo product = productInfoRepository.findByName(request.getProductName())
        .orElseThrow(() -> new ProductInfoException(
            String.format(ExceptionMessages.PRODUCT_INFO_NOT_FOUND_BY_NAME,
                request.getProductName())));

    Reservation reservation = reservationRepository.findById(request.getReservationId())
        .orElseThrow(() -> new ReservationException(
            String.format(ExceptionMessages.RESERVATION_NOT_FOUND_BY_ID,
                request.getReservationId())));

    return ReservedProduct.builder()
        .id(UUID.randomUUID())
        .productInfo(product)
        .quantity(request.getQuantity())
        .reservation(reservation)
        .build();
  }

  @Override
  public ReservedProductRequest mapEntityToRequest(ReservedProduct entity) {
    return null;
  }

  @Override
  public ReservedProduct mapResponseToEntity(ReservedProductResponse response) {
    return null;
  }

  @Override
  public ReservedProductResponse mapEntityToResponse(ReservedProduct entity) {
    return ReservedProductResponse.builder()
        .productInfo(productInfoMapper.mapEntityToResponse(entity.getProductInfo()))
        .quantity(entity.getQuantity())
        .build();
  }
}

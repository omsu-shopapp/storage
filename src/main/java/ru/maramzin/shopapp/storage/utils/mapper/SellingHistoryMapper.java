package ru.maramzin.shopapp.storage.utils.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.storage.data.dto.history.selling.SellingHistoryRequest;
import ru.maramzin.shopapp.storage.data.dto.history.selling.SellingHistoryResponse;
import ru.maramzin.shopapp.storage.data.entity.SellingHistory;

/**
 * The type Selling history mapper.
 */
@RequiredArgsConstructor
@Component
public class SellingHistoryMapper implements
    EntityMapper<SellingHistory, SellingHistoryRequest, SellingHistoryResponse> {

  private final ObjectMapper mapper;

  @Override
  public SellingHistory mapRequestToEntity(SellingHistoryRequest request) {
    return mapper.convertValue(request, SellingHistory.class);
  }

  @Override
  public SellingHistoryRequest mapEntityToRequest(SellingHistory entity) {
    return mapper.convertValue(entity, SellingHistoryRequest.class);
  }

  @Override
  public SellingHistory mapResponseToEntity(SellingHistoryResponse response) {
    return mapper.convertValue(response, SellingHistory.class);
  }

  @Override
  public SellingHistoryResponse mapEntityToResponse(SellingHistory entity) {
    return mapper.convertValue(entity, SellingHistoryResponse.class);
  }
}

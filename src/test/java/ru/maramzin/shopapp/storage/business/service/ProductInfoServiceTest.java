package ru.maramzin.shopapp.storage.business.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.util.Pair;
import ru.maramzin.shopapp.storage.data.dto.operation.filter.FilterRequest;
import ru.maramzin.shopapp.storage.data.dto.product.ProductInfoRequest;
import ru.maramzin.shopapp.storage.data.dto.product.ProductInfoResponse;
import ru.maramzin.shopapp.storage.data.entity.ProductInfo;
import ru.maramzin.shopapp.storage.data.entity.enumeration.ClothesCategory;
import ru.maramzin.shopapp.storage.data.entity.enumeration.SeasonCategory;
import ru.maramzin.shopapp.storage.data.repository.ProductInfoRepository;
import ru.maramzin.shopapp.storage.utils.exception.ProductInfoException;
import ru.maramzin.shopapp.storage.utils.exception.ValidationException;
import ru.maramzin.shopapp.storage.utils.mapper.ProductInfoMapper;

@ExtendWith(MockitoExtension.class)
class ProductInfoServiceTest {

  @Mock
  private ProductInfoMapper mapper;
  @Mock
  private ProductInfoRepository repository;
  @InjectMocks
  private ProductInfoService service;

  private static ProductInfoRequest testRequest;
  private static ProductInfo testProductInfo;
  private static ProductInfoResponse testResponse;

  @BeforeEach
  void setUp() {
    UUID sellerId = UUID.randomUUID();
    testRequest = ProductInfoRequest.builder()
        .name("test")
        .description("test")
        .cost(100)
        .discount(0)
        .quantity(10)
        .clothesCategory(ClothesCategory.HAT)
        .seasonCategory(SeasonCategory.WINTER)
        .sellerId(sellerId)
        .build();

    UUID productId = UUID.randomUUID();
    testProductInfo = ProductInfo.builder()
        .id(productId)
        .name("test")
        .description("test")
        .cost(100)
        .discount(0)
        .quantity(10)
        .clothesCategory(ClothesCategory.HAT)
        .seasonCategory(SeasonCategory.WINTER)
        .sellerId(sellerId)
        .build();

    testResponse = ProductInfoResponse.builder()
        .id(productId)
        .name("test")
        .description("test")
        .cost(100)
        .discount(0)
        .quantity(10)
        .clothesCategory(ClothesCategory.HAT)
        .seasonCategory(SeasonCategory.WINTER)
        .sellerId(sellerId)
        .build();
  }

  @Test
  void add_shouldWorkCorrectly() {
    Mockito.when(mapper.mapRequestToEntity(testRequest)).thenReturn(testProductInfo);
    Mockito.when(mapper.mapEntityToResponse(testProductInfo)).thenReturn(testResponse);
    Mockito.when(repository.save(testProductInfo)).thenReturn(testProductInfo);

    ProductInfoResponse actual = service.add(testRequest);

    Assertions.assertThat(actual).isEqualTo(testResponse);
    Assertions.assertThat(actual.getId()).isEqualTo(testResponse.getId());
  }

  @Test
  void findByName_shouldWorkCorrectly() {
    String name = "test";
    Mockito.when(repository.findByName(name)).thenReturn(Optional.ofNullable(testProductInfo));
    Mockito.when(mapper.mapEntityToResponse(testProductInfo)).thenReturn(testResponse);

    ProductInfoResponse actual = service.findByName(name);
    Assertions.assertThat(actual).isEqualTo(testResponse);
  }

  @Test
  void findByName_shouldThrowProductInfoException_whenProductInfoDoesNotExistForName() {
    String name = "test";
    Mockito.when(repository.findByName(name)).thenReturn(Optional.empty());

    Assertions.assertThatThrownBy(() -> service.findByName(name))
        .isInstanceOf(ProductInfoException.class);
  }

  @Test
  void deleteByName_shouldWorkCorrectly() {
    String name = "test";
    Mockito.doNothing().when(repository).delete(ArgumentMatchers.any());
    Mockito.when(repository.findByName(name)).thenReturn(Optional.ofNullable(testProductInfo));
    Mockito.when(mapper.mapEntityToResponse(testProductInfo)).thenReturn(testResponse);

    ProductInfoResponse actual = service.deleteByName(name);
    Assertions.assertThat(actual).isEqualTo(testResponse);
  }

  @Test
  void deleteByName_shouldThrowProductInfoException_whenProductInfoDoesNotExistForName() {
    String name = "test";
    Mockito.when(repository.findByName(name)).thenReturn(Optional.empty());

    Assertions.assertThatThrownBy(() -> service.deleteByName(name))
        .isInstanceOf(ProductInfoException.class);
  }

  @Test
  void takeByName_shouldWorkCorrectly() {
    String name = "test";
    Integer quantity = 1;
    Integer prevQuantity = testProductInfo.getQuantity();
    Mockito.when(repository.findByName(name)).thenReturn(Optional.ofNullable(testProductInfo));
    Mockito.when(repository.save(testProductInfo)).thenReturn(testProductInfo);
    Mockito.when(mapper.mapEntityToResponse(testProductInfo)).thenReturn(testResponse);

    ProductInfoResponse actual = service.takeByName(name, quantity);
    Assertions.assertThat(actual).isEqualTo(testResponse);
    Assertions.assertThat(testProductInfo.getQuantity()).isEqualTo(prevQuantity - quantity);
  }

  @Test
  void takeByName_shouldThrowProductInfoException_whenProductInfoDoesNotExistForName() {
    String name = "test";
    Integer quantity = 1;
    Mockito.when(repository.findByName(name)).thenReturn(Optional.empty());

    Assertions.assertThatThrownBy(() -> service.takeByName(name, quantity))
        .isInstanceOf(ProductInfoException.class);
  }

  @Test
  void takeByName_shouldThrowValidationException_whenNotEnoughQuantity() {
    String name = "test";
    Integer prevQuantity = testProductInfo.getQuantity();
    Integer quantity = prevQuantity + 1;
    Mockito.when(repository.findByName(name)).thenReturn(Optional.ofNullable(testProductInfo));

    Assertions.assertThatThrownBy(() -> service.takeByName(name, quantity))
        .isInstanceOf(ValidationException.class);
  }

  @Test
  void fillByName_shouldWorkCorrectly() {
    String name = "test";
    Integer quantity = 1;
    Integer prevQuantity = testProductInfo.getQuantity();
    UUID sellerId = testProductInfo.getSellerId();
    Mockito.when(repository.findByName(name)).thenReturn(Optional.ofNullable(testProductInfo));
    Mockito.when(repository.save(testProductInfo)).thenReturn(testProductInfo);
    Mockito.when(mapper.mapEntityToResponse(testProductInfo)).thenReturn(testResponse);

    ProductInfoResponse actual = service.fillByName(name, quantity, sellerId);
    Assertions.assertThat(actual).isEqualTo(testResponse);
    Assertions.assertThat(testProductInfo.getQuantity()).isEqualTo(prevQuantity + quantity);
  }

  @Test
  void fillByName_shouldThrowProductInfoException_whenProductInfoDoesNotExistForName() {
    String name = "test";
    Integer quantity = ArgumentMatchers.any();
    Integer prevQuantity = testProductInfo.getQuantity();
    UUID sellerId = testProductInfo.getSellerId();
    Mockito.when(repository.findByName(name)).thenReturn(Optional.empty());

    Assertions.assertThatThrownBy(() -> service.fillByName(name, quantity, sellerId))
        .isInstanceOf(ProductInfoException.class);
  }

  @Test
  void fillByName_shouldThrowValidationException_whenSellerIdIsWrong() {
    String name = "test";
    Integer quantity = ArgumentMatchers.any();
    UUID sellerId = UUID.randomUUID();
    Mockito.when(repository.findByName(name)).thenReturn(Optional.ofNullable(testProductInfo));

    Assertions.assertThatThrownBy(() -> service.fillByName(name, quantity, sellerId))
        .isInstanceOf(ValidationException.class);
  }

  @Test
  void changePriceByName_shouldWorkCorrectly() {
    String name = "test";
    Integer newPrice = 10000;
    Integer prevPrice = testProductInfo.getCost();
    UUID sellerId = testProductInfo.getSellerId();
    Mockito.when(repository.findByName(name)).thenReturn(Optional.ofNullable(testProductInfo));
    Mockito.when(repository.save(testProductInfo)).thenReturn(testProductInfo);
    Mockito.when(mapper.mapEntityToResponse(testProductInfo)).thenReturn(testResponse);

    Pair<ProductInfoResponse, Integer> actual = service.changePriceByName(name, newPrice, sellerId);
    Assertions.assertThat(actual).isEqualTo(Pair.of(testResponse, prevPrice));
    Assertions.assertThat(testProductInfo.getCost()).isEqualTo(newPrice);
  }

  @Test
  void changePriceByName_shouldThrowProductInfoException_whenProductInfoDoesNotExistForName() {
    String name = "test";
    Integer newPrice = 10000;
    UUID sellerId = testProductInfo.getSellerId();
    Mockito.when(repository.findByName(name)).thenReturn(Optional.empty());

    Assertions.assertThatThrownBy(() -> service.changePriceByName(name, newPrice, sellerId))
        .isInstanceOf(ProductInfoException.class);
  }

  @Test
  void changePriceByName_shouldThrowValidationException_whenSellerIdIsWrong() {
    String name = "test";
    Integer newPrice = 10000;
    UUID sellerId = UUID.randomUUID();
    Mockito.when(repository.findByName(name)).thenReturn(Optional.ofNullable(testProductInfo));

    Assertions.assertThatThrownBy(() -> service.changePriceByName(name, newPrice, sellerId))
        .isInstanceOf(ValidationException.class);
  }

  @Test
  void changeDiscountByName_shouldWorkCorrectly() {
    String name = "test";
    Integer newDiscount = 10;
    Integer prevDiscount = testProductInfo.getDiscount();
    UUID sellerId = testProductInfo.getSellerId();
    Mockito.when(repository.findByName(name)).thenReturn(Optional.ofNullable(testProductInfo));
    Mockito.when(repository.save(testProductInfo)).thenReturn(testProductInfo);
    Mockito.when(mapper.mapEntityToResponse(testProductInfo)).thenReturn(testResponse);

    Pair<ProductInfoResponse, Integer> actual = service.changeDiscountByName(name, newDiscount, sellerId);
    Assertions.assertThat(actual).isEqualTo(Pair.of(testResponse, prevDiscount));
    Assertions.assertThat(testProductInfo.getDiscount()).isEqualTo(newDiscount);
  }

  @Test
  void changeDiscountByName_shouldThrowProductInfoException_whenProductInfoDoesNotExistForName() {
    String name = "test";
    Integer newDiscount = 10000;
    UUID sellerId = testProductInfo.getSellerId();
    Mockito.when(repository.findByName(name)).thenReturn(Optional.empty());

    Assertions.assertThatThrownBy(() -> service.changePriceByName(name, newDiscount, sellerId))
        .isInstanceOf(ProductInfoException.class);
  }

  @Test
  void changeDiscountByName_shouldThrowValidationException_whenSellerIdIsWrong() {
    String name = "test";
    Integer newDiscount = 10000;
    UUID sellerId = UUID.randomUUID();
    Mockito.when(repository.findByName(name)).thenReturn(Optional.ofNullable(testProductInfo));

    Assertions.assertThatThrownBy(() -> service.changePriceByName(name, newDiscount, sellerId))
        .isInstanceOf(ValidationException.class);
  }

  @Test
  void findAll_shouldWorkCorrectly() {
    Mockito.when(repository.findAll()).thenReturn(List.of(testProductInfo));
    Mockito.when(mapper.mapEntityToResponse(testProductInfo)).thenReturn(testResponse);

    List<ProductInfoResponse> expected = List.of(testResponse);
    List<ProductInfoResponse> actual = service.findAll();
    Assertions.assertThat(actual).isEqualTo(expected);
  }

  @Test
  void filter() {
    FilterRequest request = FilterRequest.builder().build();
    Mockito.when(repository.filter(ArgumentMatchers.any(),
            ArgumentMatchers.any(), ArgumentMatchers.any(),
            ArgumentMatchers.any(), ArgumentMatchers.any(),
            ArgumentMatchers.any(), ArgumentMatchers.any(),
            ArgumentMatchers.any()))
        .thenReturn(List.of(testProductInfo));
    Mockito.when(mapper.mapEntityToResponse(testProductInfo)).thenReturn(testResponse);

    List<ProductInfoResponse> expected = List.of(testResponse);
    List<ProductInfoResponse> actual = service.filter(request);
    Assertions.assertThat(actual).isEqualTo(expected);
  }
}
package ru.maramzin.shopapp.storage.business.service.history;

import java.util.List;
import java.util.UUID;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.maramzin.shopapp.storage.data.dto.history.price.PriceHistoryRequest;
import ru.maramzin.shopapp.storage.data.dto.history.price.PriceHistoryResponse;
import ru.maramzin.shopapp.storage.data.entity.PriceHistory;
import ru.maramzin.shopapp.storage.data.repository.PriceHistoryRepository;
import ru.maramzin.shopapp.storage.utils.mapper.PriceHistoryMapper;

@ExtendWith(MockitoExtension.class)
class PriceHistoryServiceTest {

  @Mock
  private PriceHistoryRepository repository;

  @Mock
  private PriceHistoryMapper mapper;

  @InjectMocks
  private PriceHistoryService service;

  @Test
  public void add_shouldWorkCorrectly() {
    UUID productInfoId = UUID.randomUUID();
    PriceHistoryRequest request = PriceHistoryRequest.builder()
        .productInfoId(productInfoId)
        .build();
    PriceHistory history = PriceHistory.builder()
        .productInfoId(productInfoId)
        .build();
    PriceHistoryResponse expected = PriceHistoryResponse.builder()
        .productInfoId(productInfoId)
        .build();
    Mockito.when(mapper.mapRequestToEntity(request)).thenReturn(history);
    Mockito.when(mapper.mapEntityToResponse(history)).thenReturn(expected);
    Mockito.when(repository.save(history)).thenReturn(history);

    PriceHistoryResponse actual = service.add(request);

    Assertions.assertThat(actual).isEqualTo(expected);
    Assertions.assertThat(actual.getProductInfoId()).isEqualTo(productInfoId);
  }

  @Test
  public void findByProductInfoId_shouldWorkCorrectly() {
    UUID productInfoId = UUID.randomUUID();
    PriceHistory history = PriceHistory.builder()
        .productInfoId(productInfoId)
        .build();
    List<PriceHistory> histories = List.of(history);
    Mockito.when(repository.findAllByProductInfoId(productInfoId)).thenReturn(histories);

    PriceHistoryResponse response = PriceHistoryResponse.builder()
        .productInfoId(productInfoId)
        .build();
    Mockito.when(mapper.mapEntityToResponse(history)).thenReturn(response);
    List<PriceHistoryResponse> expected = List.of(response);
    List<PriceHistoryResponse> actual = service.findByProductInfoId(productInfoId);

    Assertions.assertThat(actual).isEqualTo(expected);
    Assertions.assertThat(actual).hasSize(1);
  }
}
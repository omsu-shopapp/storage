package ru.maramzin.shopapp.storage.business.service.history;

import java.util.List;
import java.util.UUID;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.maramzin.shopapp.storage.data.dto.history.replenishment.ReplenishmentHistoryRequest;
import ru.maramzin.shopapp.storage.data.dto.history.replenishment.ReplenishmentHistoryResponse;
import ru.maramzin.shopapp.storage.data.entity.ReplenishmentHistory;
import ru.maramzin.shopapp.storage.data.repository.ReplenishmentHistoryRepository;
import ru.maramzin.shopapp.storage.utils.mapper.ReplenishmentHistoryMapper;

@ExtendWith(MockitoExtension.class)
class ReplenishmentHistoryServiceTest {

  @Mock
  private ReplenishmentHistoryRepository repository;

  @Mock
  private ReplenishmentHistoryMapper mapper;

  @InjectMocks
  private ReplenishmentHistoryService service;

  @Test
  public void add_shouldWorkCorrectly() {
    UUID productInfoId = UUID.randomUUID();
    ReplenishmentHistoryRequest request = ReplenishmentHistoryRequest.builder()
        .productInfoId(productInfoId)
        .build();
    ReplenishmentHistory history = ReplenishmentHistory.builder()
        .productInfoId(productInfoId)
        .build();
    ReplenishmentHistoryResponse expected = ReplenishmentHistoryResponse.builder()
        .productInfoId(productInfoId)
        .build();
    Mockito.when(mapper.mapRequestToEntity(request)).thenReturn(history);
    Mockito.when(mapper.mapEntityToResponse(history)).thenReturn(expected);
    Mockito.when(repository.save(history)).thenReturn(history);

    ReplenishmentHistoryResponse actual = service.add(request);

    Assertions.assertThat(actual).isEqualTo(expected);
    Assertions.assertThat(actual.getProductInfoId()).isEqualTo(productInfoId);
  }

  @Test
  public void findByProductInfoId_shouldWorkCorrectly() {
    UUID productInfoId = UUID.randomUUID();
    ReplenishmentHistory history = ReplenishmentHistory.builder()
        .productInfoId(productInfoId)
        .build();
    List<ReplenishmentHistory> histories = List.of(history);
    Mockito.when(repository.findAllByProductInfoId(productInfoId)).thenReturn(histories);

    ReplenishmentHistoryResponse response = ReplenishmentHistoryResponse.builder()
        .productInfoId(productInfoId)
        .build();
    Mockito.when(mapper.mapEntityToResponse(history)).thenReturn(response);
    List<ReplenishmentHistoryResponse> expected = List.of(response);
    List<ReplenishmentHistoryResponse> actual = service.findByProductInfoId(productInfoId);

    Assertions.assertThat(actual).isEqualTo(expected);
    Assertions.assertThat(actual).hasSize(1);
  }
}
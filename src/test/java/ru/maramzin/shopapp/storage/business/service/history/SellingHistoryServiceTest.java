package ru.maramzin.shopapp.storage.business.service.history;

import java.util.List;
import java.util.UUID;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.maramzin.shopapp.storage.data.dto.history.selling.SellingHistoryRequest;
import ru.maramzin.shopapp.storage.data.dto.history.selling.SellingHistoryResponse;
import ru.maramzin.shopapp.storage.data.entity.SellingHistory;
import ru.maramzin.shopapp.storage.data.repository.SellingHistoryRepository;
import ru.maramzin.shopapp.storage.utils.mapper.SellingHistoryMapper;

@ExtendWith(MockitoExtension.class)
class SellingHistoryServiceTest {

  @Mock
  private SellingHistoryRepository repository;

  @Mock
  private SellingHistoryMapper mapper;

  @InjectMocks
  private SellingHistoryService service;

  @Test
  public void add_shouldWorkCorrectly() {
    UUID productInfoId = UUID.randomUUID();
    SellingHistoryRequest request = SellingHistoryRequest.builder()
        .productInfoId(productInfoId)
        .build();
    SellingHistory history = SellingHistory.builder()
        .productInfoId(productInfoId)
        .build();
    SellingHistoryResponse expected = SellingHistoryResponse.builder()
        .productInfoId(productInfoId)
        .build();
    Mockito.when(mapper.mapRequestToEntity(request)).thenReturn(history);
    Mockito.when(mapper.mapEntityToResponse(history)).thenReturn(expected);
    Mockito.when(repository.save(history)).thenReturn(history);

    SellingHistoryResponse actual = service.add(request);

    Assertions.assertThat(actual).isEqualTo(expected);
    Assertions.assertThat(actual.getProductInfoId()).isEqualTo(productInfoId);
  }

  @Test
  public void findByProductInfoId_shouldWorkCorrectly() {
    UUID productInfoId = UUID.randomUUID();
    SellingHistory history = SellingHistory.builder()
        .productInfoId(productInfoId)
        .build();
    List<SellingHistory> histories = List.of(history);
    Mockito.when(repository.findAllByProductInfoId(productInfoId)).thenReturn(histories);

    SellingHistoryResponse response = SellingHistoryResponse.builder()
        .productInfoId(productInfoId)
        .build();
    Mockito.when(mapper.mapEntityToResponse(history)).thenReturn(response);
    List<SellingHistoryResponse> expected = List.of(response);
    List<SellingHistoryResponse> actual = service.findByProductInfoId(productInfoId);

    Assertions.assertThat(actual).isEqualTo(expected);
    Assertions.assertThat(actual).hasSize(1);
  }
}
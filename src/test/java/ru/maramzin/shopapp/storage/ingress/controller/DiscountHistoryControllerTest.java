package ru.maramzin.shopapp.storage.ingress.controller;

import java.util.List;
import java.util.UUID;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.maramzin.shopapp.storage.business.facade.ProductFacade;
import ru.maramzin.shopapp.storage.data.dto.history.discount.DiscountHistoryResponse;

@WebMvcTest(DiscountHistoryController.class)
class DiscountHistoryControllerTest {

  private static final String FIND_URL = "/histories/discount/find";

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private ProductFacade facade;

  @Test
  void find_shouldWorkCorrectly() throws Exception {
    String productName = "test";
    DiscountHistoryResponse response = DiscountHistoryResponse.builder()
        .productInfoId(UUID.randomUUID())
        .discountBefore(10)
        .discountAfter(20)
        .userId(UUID.randomUUID())
        .build();

    Mockito.when(facade.findDiscountHistoriesByProductName(productName)).thenReturn(List.of(response));

    mockMvc.perform(MockMvcRequestBuilders.get(FIND_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .param("productName", productName)
            .accept(MediaType.APPLICATION_JSON)
        ).andDo(MockMvcResultHandlers.print())
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(1)))
        .andExpect(MockMvcResultMatchers.jsonPath("$[0].productInfoId", Matchers.is(response.getProductInfoId().toString())))
        .andExpect(MockMvcResultMatchers.jsonPath("$[0].discountBefore", Matchers.is(response.getDiscountBefore())))
        .andExpect(MockMvcResultMatchers.jsonPath("$[0].discountAfter", Matchers.is(response.getDiscountAfter())))
        .andExpect(MockMvcResultMatchers.jsonPath("$[0].userId", Matchers.is(response.getUserId().toString())))

        .andReturn();

    Mockito.verify(facade, Mockito.times(1)).findDiscountHistoriesByProductName(productName);
  }
}
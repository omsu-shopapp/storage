package ru.maramzin.shopapp.storage.ingress.controller;

import java.util.List;
import java.util.UUID;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.maramzin.shopapp.storage.business.facade.ProductFacade;
import ru.maramzin.shopapp.storage.data.dto.history.price.PriceHistoryResponse;

@WebMvcTest(PriceHistoryController.class)
class PriceHistoryControllerTest {
  private static final String FIND_URL = "/histories/price/find";

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private ProductFacade facade;

  @Test
  void find_shouldWorkCorrectly() throws Exception {
    String productName = "test";
    PriceHistoryResponse response = PriceHistoryResponse.builder()
        .productInfoId(UUID.randomUUID())
        .costBefore(10)
        .costAfter(20)
        .userId(UUID.randomUUID())
        .build();

    Mockito.when(facade.findPriceHistoriesByProductName(productName)).thenReturn(List.of(response));

    mockMvc.perform(MockMvcRequestBuilders.get(FIND_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .param("productName", productName)
            .accept(MediaType.APPLICATION_JSON)
        ).andDo(MockMvcResultHandlers.print())
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(1)))
        .andExpect(MockMvcResultMatchers.jsonPath("$[0].productInfoId", Matchers.is(response.getProductInfoId().toString())))
        .andExpect(MockMvcResultMatchers.jsonPath("$[0].costBefore", Matchers.is(response.getCostBefore())))
        .andExpect(MockMvcResultMatchers.jsonPath("$[0].costAfter", Matchers.is(response.getCostAfter())))
        .andExpect(MockMvcResultMatchers.jsonPath("$[0].userId", Matchers.is(response.getUserId().toString())))
        .andReturn();

    Mockito.verify(facade, Mockito.times(1)).findPriceHistoriesByProductName(productName);
  }
}
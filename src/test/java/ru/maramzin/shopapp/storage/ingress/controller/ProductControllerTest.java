package ru.maramzin.shopapp.storage.ingress.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.maramzin.shopapp.storage.business.facade.ProductFacade;
import ru.maramzin.shopapp.storage.business.service.ProductInfoService;
import ru.maramzin.shopapp.storage.data.dto.operation.filter.FilterRequest;
import ru.maramzin.shopapp.storage.data.dto.operation.purchase.PurchaseRequest;
import ru.maramzin.shopapp.storage.data.dto.operation.purchase.PurchaseResponse;
import ru.maramzin.shopapp.storage.data.dto.product.ProductInfoRequest;
import ru.maramzin.shopapp.storage.data.dto.product.ProductInfoResponse;
import ru.maramzin.shopapp.storage.data.entity.enumeration.ClothesCategory;
import ru.maramzin.shopapp.storage.data.entity.enumeration.SeasonCategory;
import ru.maramzin.shopapp.storage.utils.exception.ProductInfoException;
import ru.maramzin.shopapp.storage.utils.logs.ExceptionMessages;

@WebMvcTest(ProductController.class)
class ProductControllerTest {

  private static final String FIND_BY_NAME_URL = "/product/find";
  private static final String ADD_URL = "/product/add";
  private static final String PURCHASE_URL = "/product/purchase";
  private static final String REPLENISH_URL = "/product/replenish";
  private static final String CHANGE_PRICE_URL = "/product/change/price";
  private static final String CHANGE_DISCOUNT_URL = "/product/change/discount";
  private static final String FILTER_URL = "/product/filter";

  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private ObjectMapper mapper;

  @MockBean
  private ProductFacade facade;

  @MockBean
  private ProductInfoService service;

  @Test
  void find_shouldWorkCorrectly_withContent() throws Exception {
    String name = "test";
    ProductInfoResponse response = ProductInfoResponse.builder()
        .id(UUID.randomUUID())
        .build();

    Mockito.when(service.findByName(name)).thenReturn(response);

    mockMvc.perform(MockMvcRequestBuilders.get(FIND_BY_NAME_URL)
        .contentType(MediaType.APPLICATION_JSON)
        .param("name", name)
        .accept(MediaType.APPLICATION_JSON)
    ).andDo(MockMvcResultHandlers.print())
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.aMapWithSize(9)))
        .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.isA(String.class)))
        .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is(response.getId().toString())))
        .andReturn();

    Mockito.verify(service, Mockito.times(1)).findByName(name);
  }

  @Test
  void find_shouldReturnStatusNotFound() throws Exception {
    String name = "test";
    String message = String.format(ExceptionMessages.PRODUCT_INFO_NOT_FOUND_BY_NAME, name);
    Mockito.doThrow(new ProductInfoException(message)).when(service).findByName(name);

    mockMvc.perform(MockMvcRequestBuilders.get(FIND_BY_NAME_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .param("name", name)
            .accept(MediaType.APPLICATION_JSON)
        ).andDo(MockMvcResultHandlers.print())
        .andExpect(MockMvcResultMatchers.status().isNotFound())
        .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.isA(String.class)))
        .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.is(message)))
        .andReturn();

    Mockito.verify(service, Mockito.times(1)).findByName(name);
  }

  @Test
  void add_shouldWorkCorrectly() throws Exception {
    String name = "test";
    Integer cost = 0;
    Integer discount = 0;
    Integer quantity = 0;
    ClothesCategory clothesCategory = ClothesCategory.HAT;
    SeasonCategory seasonCategory = SeasonCategory.WINTER;
    UUID sellerId = UUID.randomUUID();
    ProductInfoRequest request = ProductInfoRequest.builder()
        .name(name)
        .cost(cost)
        .quantity(quantity)
        .discount(discount)
        .clothesCategory(clothesCategory)
        .seasonCategory(seasonCategory)
        .sellerId(sellerId)
        .build();
    ProductInfoResponse response = ProductInfoResponse.builder()
        .id(UUID.randomUUID())
        .name(name)
        .cost(cost)
        .quantity(quantity)
        .discount(discount)
        .clothesCategory(clothesCategory)
        .seasonCategory(seasonCategory)
        .sellerId(sellerId)
        .build();

    Mockito.when(facade.addProduct(request)).thenReturn(response);

    mockMvc.perform(MockMvcRequestBuilders.put(ADD_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .content(mapper.writeValueAsString(request))
            .accept(MediaType.APPLICATION_JSON)
        ).andDo(MockMvcResultHandlers.print())
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.aMapWithSize(9)))
        .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.isA(String.class)))
        .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is(response.getId().toString())))
        .andReturn();

    Mockito.verify(facade, Mockito.times(1)).addProduct(request);
  }

  @Test
  void purchase_shouldWorkCorrectly_withContent() throws Exception {
    String name = "test";
    Integer quantity = 0;
    UUID clientId = UUID.randomUUID();
    PurchaseRequest request = PurchaseRequest.builder()
        .productName(name)
        .quantity(quantity)
        .build();
    PurchaseResponse response = PurchaseResponse.builder()
        .productId(UUID.randomUUID())
        .productName(name)
        .cost(0)
        .quantity(quantity)
        .discount(0)
        .build();

    Mockito.when(facade.purchase(List.of(request), clientId)).thenReturn(List.of(response));

    mockMvc.perform(MockMvcRequestBuilders.post(PURCHASE_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .param("clientId", clientId.toString())
            .content(mapper.writeValueAsString(List.of(request)))
            .accept(MediaType.APPLICATION_JSON)
        ).andDo(MockMvcResultHandlers.print())
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(1)))
        .andExpect(MockMvcResultMatchers.jsonPath("$[0]", Matchers.aMapWithSize(5)))
        .andExpect(MockMvcResultMatchers.jsonPath("$[0].productId", Matchers.is(response.getProductId().toString())))
        .andExpect(MockMvcResultMatchers.jsonPath("$[0].productName", Matchers.is(name)))
        .andExpect(MockMvcResultMatchers.jsonPath("$[0].cost", Matchers.is(response.getCost())))
        .andExpect(MockMvcResultMatchers.jsonPath("$[0].discount", Matchers.is(response.getDiscount())))
        .andExpect(MockMvcResultMatchers.jsonPath("$[0].quantity", Matchers.is(response.getQuantity())))
        .andReturn();

    Mockito.verify(facade, Mockito.times(1)).purchase(List.of(request), clientId);
  }

  @Test
  void purchase_shouldWorkCorrectly_withoutContent() throws Exception {
    UUID clientId = UUID.randomUUID();
    Mockito.when(facade.purchase(Collections.emptyList(), clientId)).thenReturn(Collections.emptyList());

    mockMvc.perform(MockMvcRequestBuilders.post(PURCHASE_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .param("clientId", clientId.toString())
            .content(mapper.writeValueAsString(Collections.emptyList()))
            .accept(MediaType.APPLICATION_JSON)
        ).andDo(MockMvcResultHandlers.print())
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(0)))
        .andReturn();

    Mockito.verify(facade, Mockito.times(1)).purchase(Collections.emptyList(), clientId);
  }

  @Test
  void replenish_shouldWorkCorrectly() throws Exception {
    String name = "test";
    Integer quantity = 1;
    UUID sellerId = UUID.randomUUID();
    ProductInfoResponse response = ProductInfoResponse.builder()
        .id(UUID.randomUUID())
        .name(name)
        .cost(100)
        .quantity(quantity)
        .discount(0)
        .clothesCategory(ClothesCategory.HAT)
        .seasonCategory(SeasonCategory.WINTER)
        .sellerId(sellerId)
        .build();

    Mockito.when(facade.replenishProduct(name, quantity, sellerId)).thenReturn(response);

    mockMvc.perform(MockMvcRequestBuilders.post(REPLENISH_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .param("productName", name)
            .param("quantity", quantity.toString())
            .param("sellerId", sellerId.toString())
            .accept(MediaType.APPLICATION_JSON)
        ).andDo(MockMvcResultHandlers.print())
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.aMapWithSize(9)))
        .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is(name)))
        .andExpect(MockMvcResultMatchers.jsonPath("$.quantity", Matchers.is(quantity)))
        .andExpect(MockMvcResultMatchers.jsonPath("$.sellerId", Matchers.is(sellerId.toString())))
        .andReturn();

    Mockito.verify(facade, Mockito.times(1)).replenishProduct(name, quantity, sellerId);
  }

  @Test
  void replenish_shouldReturnStatusNotFound() throws Exception {
    String name = "test";
    Integer quantity = 1;
    UUID sellerId = UUID.randomUUID();
    String message = String.format(ExceptionMessages.PRODUCT_INFO_NOT_FOUND_BY_NAME, name);
    Mockito.doThrow(new ProductInfoException(message)).when(facade).replenishProduct(name, quantity, sellerId);

    mockMvc.perform(MockMvcRequestBuilders.post(REPLENISH_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .param("productName", name)
            .param("quantity", quantity.toString())
            .param("sellerId", sellerId.toString())
            .accept(MediaType.APPLICATION_JSON)
        ).andDo(MockMvcResultHandlers.print())
        .andExpect(MockMvcResultMatchers.status().isNotFound())
        .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.isA(String.class)))
        .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.is(message)))
        .andReturn();

    Mockito.verify(facade, Mockito.times(1)).replenishProduct(name, quantity, sellerId);
  }

  @Test
  void changePrice_shouldWorkCorrectly() throws Exception {
    String name = "test";
    Integer price = 1;
    UUID sellerId = UUID.randomUUID();
    ProductInfoResponse response = ProductInfoResponse.builder()
        .id(UUID.randomUUID())
        .name(name)
        .cost(price)
        .quantity(10)
        .discount(0)
        .clothesCategory(ClothesCategory.HAT)
        .seasonCategory(SeasonCategory.WINTER)
        .sellerId(sellerId)
        .build();

    Mockito.when(facade.changePrice(name, price, sellerId)).thenReturn(response);

    mockMvc.perform(MockMvcRequestBuilders.post(CHANGE_PRICE_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .param("productName", name)
            .param("price", price.toString())
            .param("sellerId", sellerId.toString())
            .accept(MediaType.APPLICATION_JSON)
        ).andDo(MockMvcResultHandlers.print())
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.aMapWithSize(9)))
        .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is(name)))
        .andExpect(MockMvcResultMatchers.jsonPath("$.cost", Matchers.is(price)))
        .andExpect(MockMvcResultMatchers.jsonPath("$.sellerId", Matchers.is(sellerId.toString())))
        .andReturn();

    Mockito.verify(facade, Mockito.times(1)).changePrice(name, price, sellerId);
  }

  @Test
  void changePrice_shouldReturnStatusNotFound() throws Exception {
    String name = "test";
    Integer price = 1;
    UUID sellerId = UUID.randomUUID();
    String message = String.format(ExceptionMessages.PRODUCT_INFO_NOT_FOUND_BY_NAME, name);
    Mockito.doThrow(new ProductInfoException(message)).when(facade).changePrice(name, price, sellerId);

    mockMvc.perform(MockMvcRequestBuilders.post(CHANGE_PRICE_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .param("productName", name)
            .param("price", price.toString())
            .param("sellerId", sellerId.toString())
            .accept(MediaType.APPLICATION_JSON)
        ).andDo(MockMvcResultHandlers.print())
        .andExpect(MockMvcResultMatchers.status().isNotFound())
        .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.isA(String.class)))
        .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.is(message)))
        .andReturn();

    Mockito.verify(facade, Mockito.times(1)).changePrice(name, price, sellerId);
  }

  @Test
  void changeDiscount_shouldWorkCorrectly() throws Exception {
    String name = "test";
    Integer discount = 1;
    UUID sellerId = UUID.randomUUID();
    ProductInfoResponse response = ProductInfoResponse.builder()
        .id(UUID.randomUUID())
        .name(name)
        .cost(100)
        .quantity(10)
        .discount(discount)
        .clothesCategory(ClothesCategory.HAT)
        .seasonCategory(SeasonCategory.WINTER)
        .sellerId(sellerId)
        .build();

    Mockito.when(facade.changeDiscount(name, discount, sellerId)).thenReturn(response);

    mockMvc.perform(MockMvcRequestBuilders.post(CHANGE_DISCOUNT_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .param("productName", name)
            .param("discount", discount.toString())
            .param("sellerId", sellerId.toString())
            .accept(MediaType.APPLICATION_JSON)
        ).andDo(MockMvcResultHandlers.print())
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.aMapWithSize(9)))
        .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is(name)))
        .andExpect(MockMvcResultMatchers.jsonPath("$.discount", Matchers.is(discount)))
        .andExpect(MockMvcResultMatchers.jsonPath("$.sellerId", Matchers.is(sellerId.toString())))
        .andReturn();

    Mockito.verify(facade, Mockito.times(1)).changeDiscount(name, discount, sellerId);
  }

  @Test
  void changeDiscount_shouldReturnStatusNotFound() throws Exception {
    String name = "test";
    Integer discount = 1;
    UUID sellerId = UUID.randomUUID();
    String message = String.format(ExceptionMessages.PRODUCT_INFO_NOT_FOUND_BY_NAME, name);
    Mockito.doThrow(new ProductInfoException(message)).when(facade).changeDiscount(name, discount, sellerId);

    mockMvc.perform(MockMvcRequestBuilders.post(CHANGE_DISCOUNT_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .param("productName", name)
            .param("discount", discount.toString())
            .param("sellerId", sellerId.toString())
            .accept(MediaType.APPLICATION_JSON)
        ).andDo(MockMvcResultHandlers.print())
        .andExpect(MockMvcResultMatchers.status().isNotFound())
        .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.isA(String.class)))
        .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.is(message)))
        .andReturn();

    Mockito.verify(facade, Mockito.times(1)).changeDiscount(name, discount, sellerId);
  }

  @Test
  void filter_shouldWorkCorrectly() throws Exception {
    String name = "test";
    Integer discountFrom = 10;
    UUID sellerId = UUID.randomUUID();
    FilterRequest filterRequest = FilterRequest.builder()
        .name(name)
        .discountFrom(discountFrom)
        .sellerId(sellerId)
        .build();

    ProductInfoResponse response = ProductInfoResponse.builder()
        .id(UUID.randomUUID())
        .name("<<<" + name + ">>>")
        .cost(100)
        .quantity(10)
        .discount(discountFrom + 10)
        .clothesCategory(ClothesCategory.HAT)
        .seasonCategory(SeasonCategory.WINTER)
        .sellerId(sellerId)
        .build();

    Mockito.when(service.filter(filterRequest)).thenReturn(List.of(response));

    mockMvc.perform(MockMvcRequestBuilders.get(FILTER_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .param("name", name)
            .param("discountFrom", discountFrom.toString())
            .param("sellerId", sellerId.toString())
            .accept(MediaType.APPLICATION_JSON)
        ).andDo(MockMvcResultHandlers.print())
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(1)))
        .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", Matchers.containsString(name)))
        .andExpect(MockMvcResultMatchers.jsonPath("$[0].discount", Matchers.greaterThan(discountFrom)))
        .andExpect(MockMvcResultMatchers.jsonPath("$[0].sellerId", Matchers.is(sellerId.toString())))
        .andReturn();

    Mockito.verify(service, Mockito.times(1)).filter(filterRequest);
  }
}
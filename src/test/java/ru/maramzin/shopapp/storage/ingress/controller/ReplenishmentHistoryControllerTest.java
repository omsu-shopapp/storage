package ru.maramzin.shopapp.storage.ingress.controller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.maramzin.shopapp.storage.business.facade.ProductFacade;
import ru.maramzin.shopapp.storage.data.dto.history.replenishment.ReplenishmentHistoryResponse;

@WebMvcTest(ReplenishmentHistoryController.class)
class ReplenishmentHistoryControllerTest {

  private static final String FIND_URL = "/histories/replenishment/find";

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private ProductFacade facade;

  @Test
  void find_shouldWorkCorrectly() throws Exception {
    String productName = "test";
    ReplenishmentHistoryResponse response = ReplenishmentHistoryResponse.builder()
        .productInfoId(UUID.randomUUID())
        .quantity(10)
        .datetime(LocalDateTime.now())
        .userId(UUID.randomUUID())
        .build();

    Mockito.when(facade.findReplenishmentHistoriesByProductName(productName)).thenReturn(List.of(response));

    mockMvc.perform(MockMvcRequestBuilders.get(FIND_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .param("productName", productName)
            .accept(MediaType.APPLICATION_JSON)
        ).andDo(MockMvcResultHandlers.print())
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(1)))
        .andExpect(MockMvcResultMatchers.jsonPath("$[0].productInfoId", Matchers.is(response.getProductInfoId().toString())))
        .andExpect(MockMvcResultMatchers.jsonPath("$[0].quantity", Matchers.is(response.getQuantity())))
        .andExpect(MockMvcResultMatchers.jsonPath("$[0].userId", Matchers.is(response.getUserId().toString())))
        .andReturn();

    Mockito.verify(facade, Mockito.times(1)).findReplenishmentHistoriesByProductName(productName);
  }
}